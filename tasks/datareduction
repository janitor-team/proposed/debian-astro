Format: https://blends.debian.org/blends/1.1
Task: Data Reduction
Install: true
Description: Data reduction pipelines for astronomy
 The term data reduction in astronomy means the data processing from the raw
 exposures up to scientific usable data. This includes the basic CCD
 processing, the astrometric and photometric calibrations, the reprocessing,
 error estimation and other steps.
 .
 This metapackage will install astronomy data reduction pipelines that can be
 used universally, but also those for specific telescopes.

Recommends: python3-cpl, esorex,
         eso-pipelines
Suggests: cpl-plugin-amber, cpl-plugin-fors, cpl-plugin-giraf,
          cpl-plugin-hawki, cpl-plugin-sinfo, cpl-plugin-kmos,
          cpl-plugin-vimos, cpl-plugin-uves, cpl-plugin-xshoo,
          cpl-plugin-naco, cpl-plugin-visir, cpl-plugin-muse
Why: Data reduction suite for data taken with ESO instruments

Recommends: astromatic, source-extractor,
 swarp, psfex, stiff, missfits, scamp, weightwatcher

Recommends: astrometry.net

Recommends: montage, python3-montage-wrapper

Recommends: python3-astroscrappy,
 python3-ccdproc,
 python3-photutils

Recommends: fitsh

Recommends: munipack

Recommends: gnuastro

Recommends: yorick-mira

Recommends: theli
